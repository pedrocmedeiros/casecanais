### TRANSFERENCIA SERVICE

---
###### Gateway AWS: http://18.228.61.168:5000
###### Service Discovery AWS: http://18.228.61.168:8761

---
###### Collection Postman: POSTMAN/Case.postman_collection.json
###### Environment Postman: POSTMAN/case.postman_environment.json

---
#### Clientes
[Cadastra novo cliente](#post-casev1clientes) <br/>
[Busca todos clientes](#get-casev1clientes) <br/>
[Busca clientes por numero conta](#get-casev1clientesnumeroconta) <br/>

#### Transferencias
[Realiza nova transferencia](#post-casev1transferencias) <br/>
[Busca transferencias por numero conta](#get-casev1transferenciasnumeroconta) <br/>

---
### POST /case/v1/clientes
**Exemplo requisicao**
```
{
    "nome": "Pedro",
    "saldo": 1000.00
}
```
**Exemplo resposta [201 Created]**
```
{
    "id": 1,
    "nome": "Pedro",
    "numeroConta": "2002374663926366536",
    "saldo": 1000.0
}
```
### GET /case/v1/clientes
**Exemplo resposta [200 OK]**
```
[
    {
        "id": 1,
        "nome": "Pedro",
        "numeroConta": "2002374663926366536",
        "saldo": 1000.0
    },
    {
        "id": 2,
        "nome": "Carlos",
        "numeroConta": "1275840628761090480",
        "saldo": 2000.0
    }
]
```
### GET /case/v1/clientes/{numeroConta}
**Exemplo resposta [200 OK]**
```
{
    "id": 1,
    "nome": "Pedro",
    "numeroConta": "2002374663926366536",
    "saldo": 1000.0
}
```
---
### POST /case/v1/transferencias
**Exemplo requisicao**
```
{
    "numeroContaOrigem": {numeroContaOrigem},
    "numeroContaDestino": {numeroContaDestino},
    "valor": 10.00
}
```
**Exemplo resposta [201 Created]**
```
{
    "id": 1,
    "numeroContaOrigem": "2002374663926366536",
    "numeroContaDestino": "1275840628761090480",
    "valor": 10.0,
    "data": "16-12-2020 14:34:24",
    "status": "Processando",
    "mensagem": "Processando"
}
```
### GET /case/v1/transferencias/{numeroConta}
**Exemplo resposta [200 OK]**
```
[
    {
        "id": 2,
        "numeroContaOrigem": "2002374663926366536",
        "numeroContaDestino": "1275840628761090480",
        "valor": 10.0,
        "data": "16-12-2020 14:44:31",
        "status": "Sucesso",
        "mensagem": "Sucesso"
    },
    {
        "id": 1,
        "numeroContaOrigem": "2002374663926366536",
        "numeroContaDestino": "1275840628761090480",
        "valor": 10.0,
        "data": "16-12-2020 14:34:24",
        "status": "Sucesso",
        "mensagem": "Sucesso"
    }
]
```