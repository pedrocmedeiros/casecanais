package com.itau.caseauto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TransferenciaAcimaDoLimiteException extends RuntimeException {
    public TransferenciaAcimaDoLimiteException(Double valorTransf, Double valorMaximo) {
        super(String.format("Transferencia de %s esta acima do valor maximo de %s", valorTransf, valorMaximo));
    }
}
