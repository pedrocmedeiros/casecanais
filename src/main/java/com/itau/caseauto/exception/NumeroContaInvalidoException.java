package com.itau.caseauto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NumeroContaInvalidoException extends RuntimeException {
    public NumeroContaInvalidoException(String numeroConta) {
        super(String.format("Numero da conta '%s' invalido", numeroConta));
    }
}
