package com.itau.caseauto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
public class SaldoIndisponivelException extends RuntimeException {
    public SaldoIndisponivelException(String numeroConta, Double saldoCliente) {
        super(String.format("Cliente %s sem saldo para a transferencia, saldo disponivel: %s", numeroConta, saldoCliente));
    }
}
