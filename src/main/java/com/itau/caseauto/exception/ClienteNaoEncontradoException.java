package com.itau.caseauto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClienteNaoEncontradoException extends RuntimeException {
    public ClienteNaoEncontradoException(String numeroConta) {
        super(String.format("Cliente com numero de conta '%s' nao encontrado", numeroConta));
    }
}
