package com.itau.caseauto.repository;

import com.itau.caseauto.model.Transferencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferenciaRepository extends JpaRepository<Transferencia, Long> {

    @Query("SELECT t FROM Transferencia t WHERE t.numeroContaOrigem= ?1 or t.numeroContaDestino = ?1 ORDER BY t.data DESC")
    public List<Transferencia> findAllByNumeroConta(String numeroConta);


}