package com.itau.caseauto.controller;

import com.itau.caseauto.model.Cliente;
import com.itau.caseauto.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> cadastrar(@Valid @RequestBody Cliente novoCliente) {
        Cliente clienteCriado = clienteService.criar(novoCliente);
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteCriado);
    }

    @GetMapping
    public ResponseEntity<List<Cliente>> buscarTodos() {
        return ResponseEntity.ok().body(clienteService.buscar());
    }

    @GetMapping("/{numeroConta}")
    public ResponseEntity<Cliente> buscaClientePorNumeroConta(@PathVariable("numeroConta") String numeroConta) {
        return ResponseEntity.ok().body(clienteService.buscarPorNumeroConta(numeroConta));
    }
}
