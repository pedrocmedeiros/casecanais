package com.itau.caseauto.controller;

import com.itau.caseauto.model.Transferencia;
import com.itau.caseauto.service.TransferenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/transferencias")
public class TransferenciaController {

    @Autowired
    private TransferenciaService transferenciaService;

    @PostMapping
    public ResponseEntity<Transferencia> criar(@Valid @RequestBody Transferencia novaTransf) {
        Transferencia transferencia = transferenciaService.iniciar(novaTransf);
        return ResponseEntity.status(HttpStatus.CREATED).body(transferencia);
    }

    @GetMapping("/{numeroConta}")
    public ResponseEntity<List<Transferencia>> buscaTransferenciasPorNumeroConta(@PathVariable("numeroConta") String numeroConta) {
        return ResponseEntity.ok().body(transferenciaService.buscarPorNumeroConta(numeroConta));
    }
}
