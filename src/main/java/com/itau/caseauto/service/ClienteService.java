package com.itau.caseauto.service;

import com.itau.caseauto.exception.ClienteNaoEncontradoException;
import com.itau.caseauto.model.Cliente;
import com.itau.caseauto.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    public Cliente criar(Cliente novoCliente) {
        Cliente cliente = repository.save(novoCliente);
        String nuneroConta = gerarNumeroConta();
        cliente.setNumeroConta(nuneroConta);
        return repository.save(cliente);
    }

    public List<Cliente> buscar() {
        return repository.findAll();
    }

    public Cliente buscarPorNumeroConta(String numeroConta) {
        return repository.findByNumeroConta(numeroConta)
                .orElseThrow(() -> new ClienteNaoEncontradoException(numeroConta));
    }

    public void adicionarSaldo(String numeroConta, Double valor) {
        Cliente cliente = buscarPorNumeroConta(numeroConta);
        Double saldoAtual = cliente.getSaldo();
        cliente.setSaldo(saldoAtual + valor);
        repository.save(cliente);
    }

    public void retirarSaldo(String numeroConta, Double valor) {
        Cliente cliente = buscarPorNumeroConta(numeroConta);
        Double saldoAtual = cliente.getSaldo();
        cliente.setSaldo(saldoAtual - valor);
        repository.save(cliente);
    }

    public String gerarNumeroConta() {
        return String.valueOf(Math.abs(new Random().nextLong()));
    }
}
