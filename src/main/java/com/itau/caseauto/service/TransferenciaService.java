package com.itau.caseauto.service;

import com.itau.caseauto.exception.ClienteNaoEncontradoException;
import com.itau.caseauto.exception.NumeroContaInvalidoException;
import com.itau.caseauto.exception.SaldoIndisponivelException;
import com.itau.caseauto.exception.TransferenciaAcimaDoLimiteException;
import com.itau.caseauto.model.Cliente;
import com.itau.caseauto.model.Transferencia;
import com.itau.caseauto.repository.TransferenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransferenciaService {

    private static final Double LIMITE_TRANSFERENCIA = 1000.00;
    private static final String PROCESSANDO = "Processando";
    private static final String SUCESSO = "Sucesso";
    private static final String FALHA = "Falha";

    @Autowired
    private TransferenciaRepository repository;

    @Autowired
    private ClienteService clienteService;

    public Transferencia iniciar(Transferencia novaTransf) {
        novaTransf.setStatus(PROCESSANDO);
        novaTransf.setMensagem(PROCESSANDO);
        ControleTransferencia.novaTransferencia(new TaskTransferencia(novaTransf, this));
        return repository.save(novaTransf);
    }

    public Transferencia criar(Transferencia novaTransf) {
        String numeroContaOrigem = novaTransf.getNumeroContaOrigem();
        String numeroContaDestino = novaTransf.getNumeroContaDestino();

        validarExistenciaClientes(novaTransf, numeroContaOrigem, numeroContaDestino);
        validarValorLimiteTransferencia(novaTransf);
        validarSaldoContaOrigem(novaTransf, clienteService.buscarPorNumeroConta(numeroContaOrigem));

        return efetivarTransferencia(novaTransf, numeroContaOrigem, numeroContaDestino);
    }

    private Transferencia efetivarTransferencia(Transferencia novaTransf, String numeroContaOrigem, String numeroContaDestino) {
        clienteService.retirarSaldo(numeroContaOrigem, novaTransf.getValor());
        clienteService.adicionarSaldo(numeroContaDestino, novaTransf.getValor());
        novaTransf.setStatus(SUCESSO);
        novaTransf.setMensagem(SUCESSO);
        return repository.save(novaTransf);
    }

    //Valida se saldo do cliente origem é suficiente para a transferencia
    private void validarSaldoContaOrigem(Transferencia novaTransf, Cliente clienteOrigem) {
        if (novaTransf.getValor() > clienteOrigem.getSaldo()) {
            SaldoIndisponivelException exception = new SaldoIndisponivelException(clienteOrigem.getNumeroConta(), clienteOrigem.getSaldo());
            novaTransf.setStatus(FALHA);
            novaTransf.setMensagem(exception.getLocalizedMessage());
            repository.save(novaTransf);
            throw exception;
        }
    }

    //Valida se cliente origem e cliente destino existem
    private void validarExistenciaClientes(Transferencia novaTransf, String numeroContaOrigem, String numeroContaDestino) {
        try {
            clienteService.buscarPorNumeroConta(numeroContaOrigem);
            clienteService.buscarPorNumeroConta(numeroContaDestino);
        } catch (ClienteNaoEncontradoException e) {
            novaTransf.setStatus(FALHA);
            novaTransf.setMensagem(e.getLocalizedMessage());
            repository.save(novaTransf);
            throw e;
        }
    }

    //Valida se valor da transferencia esta dentro do limite permitido
    private void validarValorLimiteTransferencia(Transferencia novaTransf) {
        if (novaTransf.getValor() > LIMITE_TRANSFERENCIA) {
            TransferenciaAcimaDoLimiteException exception = new TransferenciaAcimaDoLimiteException(novaTransf.getValor(), LIMITE_TRANSFERENCIA);
            novaTransf.setStatus(FALHA);
            novaTransf.setMensagem(exception.getLocalizedMessage());
            repository.save(novaTransf);
            throw exception;
        }
    }

    public List<Transferencia> buscarPorNumeroConta(String numeroConta) {
        return repository.findAllByNumeroConta(numeroConta);
    }
}
