package com.itau.caseauto.service;

import com.itau.caseauto.model.Transferencia;
import org.springframework.stereotype.Component;

@Component
public class TaskTransferencia implements Runnable {

    private TransferenciaService transferenciaService;

    private Transferencia transferencia;

    public TaskTransferencia() {
    }

    public TaskTransferencia(Transferencia transferencia, TransferenciaService transferenciaService) {
        this.transferencia = transferencia;
        this.transferenciaService = transferenciaService;
    }

    @Override
    public void run() {
        transferenciaService.criar(transferencia);
    }

    public TransferenciaService getTransferenciaService() {
        return transferenciaService;
    }

    public void setTransferenciaService(TransferenciaService transferenciaService) {
        this.transferenciaService = transferenciaService;
    }

    public Transferencia getTransferencia() {
        return transferencia;
    }
}
