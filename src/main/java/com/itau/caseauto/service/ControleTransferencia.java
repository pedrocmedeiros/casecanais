package com.itau.caseauto.service;

import java.util.concurrent.*;

/*
Classe responsável por executar as transferencias de forma assincrona em multi threads
 */
public class ControleTransferencia {
    private static final ThreadPoolExecutor executor = new ThreadPoolExecutor(100, 100, 100, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(10));

    public static void novaTransferencia(TaskTransferencia task) {
        run(task);
    }

    private synchronized static void run(TaskTransferencia task) {
        CompletableFuture
                .runAsync(task, executor)
                .exceptionally(throwable -> {
                    throwable.printStackTrace();
                    throw new CompletionException(throwable);
                });
    }

}
