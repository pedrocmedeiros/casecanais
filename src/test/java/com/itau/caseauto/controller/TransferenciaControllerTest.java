package com.itau.caseauto.controller;

import com.itau.caseauto.model.Cliente;
import com.itau.caseauto.model.Transferencia;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static net.minidev.json.JSONValue.toJSONString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TransferenciaControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void criarTransferenciaSucesso() throws Exception {
        //Dado
        Double saldoInicialOrigem = 1000.00;
        Double saldoInicialDestino = 1000.00;
        Double valorTransferencia = 500.00;

        String numContaOrigem = criarCliente("Pedro", saldoInicialOrigem);
        String numContaDestino = criarCliente("Carlos", saldoInicialDestino);

        //Quando
        mvc.perform(post("/v1/transferencias")
                .content(toJSONString(new Transferencia(numContaOrigem, numContaDestino, valorTransferencia)))
                .contentType(MediaType.APPLICATION_JSON));


        //tempo para esperar a execucao assincrona da transferencia
        Thread.sleep(250);

        Cliente clienteOrigem = buscarCliente(numContaOrigem);
        Cliente clienteDestino = buscarCliente(numContaDestino);

        assertEquals(saldoInicialOrigem - valorTransferencia, clienteOrigem.getSaldo(), 0);
        assertEquals(saldoInicialDestino + valorTransferencia, clienteDestino.getSaldo(), 0);
    }

    @Test
    public void criarVariasTransferenciasSucesso() throws Exception {
        //Dado
        Double saldoInicialOrigem = 1000.00;
        Double saldoInicialDestino = 1000.00;
        Double valorTransferencia = 100.00;

        String numContaOrigem = criarCliente("Pedro", saldoInicialOrigem);
        String numContaDestino = criarCliente("Carlos", saldoInicialDestino);

        //Quando
        mvc.perform(post("/v1/transferencias")
                .content(toJSONString(new Transferencia(numContaOrigem, numContaDestino, valorTransferencia)))
                .contentType(MediaType.APPLICATION_JSON));

        Thread.sleep(250);

        //Quando
        mvc.perform(post("/v1/transferencias")
                .content(toJSONString(new Transferencia(numContaOrigem, numContaDestino, valorTransferencia)))
                .contentType(MediaType.APPLICATION_JSON));

        Thread.sleep(250);


        Cliente clienteOrigem = buscarCliente(numContaOrigem);
        Cliente clienteDestino = buscarCliente(numContaDestino);

        assertEquals(saldoInicialOrigem - (valorTransferencia * 2), clienteOrigem.getSaldo(), 0);
        assertEquals(saldoInicialDestino + (valorTransferencia * 2), clienteDestino.getSaldo(), 0);
    }

    @Test
    public void criarTransferenciaSaldoInsuficiente() throws Exception {
        //Dado
        Double saldoInicialOrigem = 100.00;
        Double saldoInicialDestino = 1000.00;
        Double valorTransferencia = 500.00;

        String numContaOrigem = criarCliente("Pedro", saldoInicialOrigem);
        String numContaDestino = criarCliente("Carlos", saldoInicialDestino);

        //Quando
        mvc.perform(post("/v1/transferencias")
                .content(toJSONString(new Transferencia(numContaOrigem, numContaDestino, valorTransferencia)))
                .contentType(MediaType.APPLICATION_JSON));

        //tempo para esperar a execucao assincrona da transferencia
        Thread.sleep(250);

        //Entao
        mvc.perform(get("/v1/transferencias/" + numContaOrigem)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].numeroContaOrigem", is(numContaOrigem)))
                .andExpect(jsonPath("$[0].status", is("Falha")));

    }

    @Test
    public void criarTransferenciaComValorAcimaDoLimite() throws Exception {
        //Dado
        Double saldoInicialOrigem = 100.00;
        Double saldoInicialDestino = 1000.00;
        Double valorTransferencia = 1000.01;

        String numContaOrigem = criarCliente("Pedro", saldoInicialOrigem);
        String numContaDestino = criarCliente("Carlos", saldoInicialDestino);

        //Quando
        mvc.perform(post("/v1/transferencias")
                .content(toJSONString(new Transferencia(numContaOrigem, numContaDestino, valorTransferencia)))
                .contentType(MediaType.APPLICATION_JSON));

        //tempo para esperar a execucao assincrona da transferencia
        Thread.sleep(250);

        //Entao
        mvc.perform(get("/v1/transferencias/" + numContaOrigem)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].numeroContaOrigem", is(numContaOrigem)))
                .andExpect(jsonPath("$[0].status", is("Falha")));

    }

    private String criarCliente(String nome, Double saldo) throws Exception {
        MvcResult result = mvc.perform(post("/v1/clientes")
                .content(toJSONString(new Cliente(nome, saldo)))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        org.json.JSONObject responseBody = new org.json.JSONObject(result.getResponse().getContentAsString());
        return responseBody.getString("numeroConta");
    }

    private Cliente buscarCliente(String numeroConta) throws Exception {
        MvcResult result = mvc.perform(get("/v1/clientes/" + numeroConta)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();

        org.json.JSONObject responseBody = new org.json.JSONObject(result.getResponse().getContentAsString());
        return parseJSONToCliente(responseBody);
    }

    private Cliente parseJSONToCliente(JSONObject responseBody) throws JSONException {
        Cliente cliente = new Cliente();
        cliente.setId(responseBody.getLong("id"));
        cliente.setNome(responseBody.getString("nome"));
        cliente.setNumeroConta(responseBody.getString("numeroConta"));
        cliente.setSaldo(responseBody.getDouble("saldo"));
        return cliente;
    }

}