package com.itau.caseauto.controller;

import com.itau.caseauto.model.Cliente;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static net.minidev.json.JSONValue.toJSONString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ClienteControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void criarClienteSucesso() throws Exception {

        mvc.perform(post("/v1/clientes")
                .content(toJSONString(new Cliente("pedro", 2000.00)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is("pedro")))
                .andExpect(jsonPath("$.saldo", is(2000.00)));
    }


    @Test
    public void criarEBuscarClientesSucesso() throws Exception {

        mvc.perform(post("/v1/clientes")
                .content(toJSONString(new Cliente("pedro", 2000.00)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is("pedro")))
                .andExpect(jsonPath("$.saldo", is(2000.00)));

        mvc.perform(post("/v1/clientes")
                .content(toJSONString(new Cliente("paulo", 12000.00)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is("paulo")))
                .andExpect(jsonPath("$.saldo", is(12000.00)));


        mvc.perform(get("/v1/clientes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void criarEBuscarPorNumeroConta() throws Exception {

        MvcResult result = mvc.perform(post("/v1/clientes")
                .content(toJSONString(new Cliente("pedro", 2000.00)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nome", is("pedro")))
                .andExpect(jsonPath("$.saldo", is(2000.00)))
                .andReturn();

        org.json.JSONObject responseBody = new org.json.JSONObject(result.getResponse().getContentAsString());

        Object numeroConta = responseBody.get("numeroConta");

        mvc.perform(get("/v1/clientes/" + numeroConta)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is("pedro")))
                .andExpect(jsonPath("$.saldo", is(2000.00)));
    }

    @Test
    public void buscarNumeroContaInexistente() throws Exception {
        mvc.perform(get("/v1/clientes/" + 111111111L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}