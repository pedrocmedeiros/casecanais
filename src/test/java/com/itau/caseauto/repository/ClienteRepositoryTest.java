package com.itau.caseauto.repository;

import com.itau.caseauto.model.Cliente;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private ClienteRepository repository;

    @Before
    public void setUp() {
        manager.clear();
    }

    @Test
    public void testCriarEBuscarCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("Abc");

        manager.persist(cliente);
        manager.flush();

        Cliente buscado = repository.findAll().get(0);

        assertEquals(cliente.getNome(), buscado.getNome());
    }

    @Test
    public void testCriarEBuscarVariosClientes() {
        Cliente cliente1 = new Cliente();
        cliente1.setNome("abc1");

        Cliente cliente2 = new Cliente();
        cliente2.setNome("abc2");

        manager.persist(cliente1);
        manager.persist(cliente2);
        manager.flush();

        List<Cliente> clientes = repository.findAll();

        assertEquals(2, clientes.size());
        assertEquals(cliente1.getNome(), clientes.get(0).getNome());
        assertEquals(cliente2.getNome(), clientes.get(1).getNome());
    }

    @Test
    public void testBuscarListaVaziaClientes() {
        List<Cliente> clientes = repository.findAll();

        assertEquals(0, clientes.size());
    }
}