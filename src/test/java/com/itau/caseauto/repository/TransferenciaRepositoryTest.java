package com.itau.caseauto.repository;

import com.itau.caseauto.model.Transferencia;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransferenciaRepositoryTest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private TransferenciaRepository repository;

    @Before
    public void setUp() {
        manager.clear();
    }

    @Test
    public void testCriarEBuscarTransferencia() {
        Transferencia transferencia = new Transferencia();
        transferencia.setValor(200.00);

        manager.persist(transferencia);
        manager.flush();

        Transferencia buscado = repository.findAll().get(0);

        assertEquals(transferencia.getValor(), buscado.getValor());
    }

    @Test
    public void testCriarEBuscarVariasTransferencias() {
        Transferencia transferencia1 = new Transferencia();
        transferencia1.setValor(200.00);

        Transferencia transferencia2 = new Transferencia();
        transferencia2.setValor(200.00);

        manager.persist(transferencia1);
        manager.persist(transferencia2);
        manager.flush();

        List<Transferencia> transferencias = repository.findAll();

        assertEquals(2, transferencias.size());
        assertEquals(transferencia1.getValor(), transferencias.get(0).getValor());
        assertEquals(transferencia2.getValor(), transferencias.get(1).getValor());
    }

    @Test
    public void testBuscarListaVaziaTransferencias() {
        List<Transferencia> transferencias = repository.findAll();

        assertEquals(0, transferencias.size());
    }
}