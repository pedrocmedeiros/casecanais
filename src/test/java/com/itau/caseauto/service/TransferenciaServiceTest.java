package com.itau.caseauto.service;

import com.itau.caseauto.exception.SaldoIndisponivelException;
import com.itau.caseauto.exception.TransferenciaAcimaDoLimiteException;
import com.itau.caseauto.model.Cliente;
import com.itau.caseauto.model.Transferencia;
import com.itau.caseauto.repository.TransferenciaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferenciaServiceTest {

    @Mock
    private ClienteService clienteService;

    @InjectMocks
    private TransferenciaService transferenciaService;

    @Mock
    private TransferenciaRepository repository;

    @Test
    public void testCriarTransferenciaComSucesso() {
        Cliente clienteOrigem = new Cliente();
        clienteOrigem.setNumeroConta("11111");
        clienteOrigem.setSaldo(1000.00);
        Cliente clienteDestino = new Cliente();
        clienteDestino.setNumeroConta("22222");
        clienteDestino.setSaldo(1000.00);

        when(clienteService.buscarPorNumeroConta(clienteOrigem.getNumeroConta()))
                .thenReturn(clienteOrigem);

        Transferencia transferencia = new Transferencia();
        transferencia.setNumeroContaOrigem(clienteOrigem.getNumeroConta());
        transferencia.setNumeroContaDestino(clienteDestino.getNumeroConta());
        transferencia.setValor(200.00);

        when(repository.save(transferencia))
                .thenReturn(transferencia);

        Transferencia novaTransf = transferenciaService.criar(transferencia);

        assertEquals("Sucesso", novaTransf.getStatus());
    }

    @Test(expected = SaldoIndisponivelException.class)
    public void testCriarTransferenciaSaldoInsuficiente() {
        Cliente clienteOrigem = new Cliente();
        clienteOrigem.setNumeroConta("11111");
        clienteOrigem.setSaldo(200.00);

        when(clienteService.buscarPorNumeroConta(clienteOrigem.getNumeroConta()))
                .thenReturn(clienteOrigem);

        Transferencia transferencia = new Transferencia();
        transferencia.setNumeroContaOrigem(clienteOrigem.getNumeroConta());
        transferencia.setValor(500.00);

        Transferencia novaTransf = transferenciaService.criar(transferencia);

        assertEquals("Falha", novaTransf.getStatus());
    }

    @Test(expected = TransferenciaAcimaDoLimiteException.class)
    public void testCriarTransferenciaValorAcimaDoLimite() {
        Transferencia transferencia = new Transferencia();
        transferencia.setValor(1001.00);

        Transferencia novaTransf = transferenciaService.criar(transferencia);

        assertEquals("Falha", novaTransf.getStatus());
    }

    @Test
    public void testCriarEBuscarVariasTransferencias() {
        Cliente clienteOrigem = new Cliente();
        clienteOrigem.setNumeroConta("11111");
        clienteOrigem.setSaldo(1000.00);
        Cliente clienteDestino = new Cliente();
        clienteOrigem.setNumeroConta("22222");
        clienteDestino.setSaldo(1000.00);

        when(clienteService.buscarPorNumeroConta(clienteOrigem.getNumeroConta()))
                .thenReturn(clienteOrigem);

        Transferencia t1 = new Transferencia();
        t1.setNumeroContaOrigem(clienteOrigem.getNumeroConta());
        t1.setNumeroContaDestino(clienteDestino.getNumeroConta());
        t1.setValor(100.00);

        Transferencia t2 = new Transferencia();
        t2.setNumeroContaOrigem(clienteOrigem.getNumeroConta());
        t2.setNumeroContaDestino(clienteDestino.getNumeroConta());
        t2.setValor(100.00);

        Transferencia t3 = new Transferencia();
        t3.setNumeroContaOrigem(clienteOrigem.getNumeroConta());
        t3.setNumeroContaDestino(clienteDestino.getNumeroConta());
        t3.setValor(100.00);

        when(repository.save(t1))
                .thenReturn(t1);
        when(repository.save(t2))
                .thenReturn(t2);
        when(repository.save(t3))
                .thenReturn(t3);

        Transferencia novaTransf1 = transferenciaService.criar(t1);
        Transferencia novaTransf2 = transferenciaService.criar(t2);
        Transferencia novaTransf3 = transferenciaService.criar(t3);

        assertEquals("Sucesso", novaTransf1.getStatus());
        assertEquals("Sucesso", novaTransf2.getStatus());
        assertEquals("Sucesso", novaTransf3.getStatus());
    }
}