package com.itau.caseauto.service;

import com.itau.caseauto.model.Cliente;
import com.itau.caseauto.repository.ClienteRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ClienteServiceTest {

    @TestConfiguration
    static class ClienteServiceTestContextConfiguration {

        @Bean
        public ClienteService clienteService() {
            return new ClienteService();
        }
    }

    @Autowired
    private ClienteService service;

    @MockBean
    private ClienteRepository repository;


    @Test
    public void testCriarCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("Abc");

        when(repository.save(cliente))
                .thenReturn(cliente);

        Cliente criado = service.criar(cliente);

        assertEquals(cliente.getNome(), criado.getNome());
    }

    @Test
    public void testBuscarCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("Abc");

        when(repository.findAll())
                .thenReturn(Collections.singletonList(cliente));

        Cliente buscado = service.buscar().get(0);

        assertEquals(cliente.getNome(), buscado.getNome());
    }

    @Test
    public void testBuscarVariosClientes() {
        Cliente cliente1 = new Cliente();
        cliente1.setNome("abc1");
        Cliente cliente2 = new Cliente();
        cliente2.setNome("abc2");

        when(repository.findAll())
                .thenReturn(Arrays.asList(cliente1, cliente2));

        List<Cliente> clientes = repository.findAll();
        Cliente buscado1 = clientes.get(0);
        Cliente buscado2 = clientes.get(1);

        assertEquals(2, clientes.size());
        assertEquals(cliente1.getNome(), buscado1.getNome());
        assertEquals(cliente2.getNome(), buscado2.getNome());
    }

    @Test
    public void testBuscarListaVaziaClientes() {

        when(repository.findAll())
                .thenReturn(Collections.emptyList());

        List<Cliente> clientes = service.buscar();

        assertEquals(0, clientes.size());
    }

    @Test
    public void testGerarNumeroConta() {
        String numeroConta = service.gerarNumeroConta();
        assertTrue(Long.parseLong(numeroConta) > 0);
    }

}