package com.itau.caseauto;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtils {

    private static String toJSONString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
